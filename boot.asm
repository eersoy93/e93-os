[org 0x7C00]

[bits 16]

start:
    cli
    xor ax, ax
    mov ds, ax
    mov es, ax
    mov ss, ax
    mov sp, 0x7C00
    sti

    mov si, message
    call print

print:
    lodsb
    or al, al
    jz hang
    mov ah, 0x0E
    int 0x10
    jmp print

hang:
    hlt
    jmp hang

message db "Welcome to E93-OS!", 0

times 510-($-$$) db 0
dw 0xAA55
