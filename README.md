# E93-OS
A simple 32-bit operating system for PCs. Currently it is a very simple bootloader only.

# Author
Erdem Ersoy (eersoy93) (with the help of ChatGPT)

# Copyright and License
Copyright (c) 2023 Erdem Ersoy (eersoy93)

Licensed with MIT license. See LICENSE file for full text.
